Boat on the river

[Intro]
Am          F           G           Am
00xxxx00xxxx00xxxx00xxxx00xxxx00xxxx00xxxx00xxxx

[Verse 1]
Am                 F
   Did you see the lights
           G        Am
   As they fell all around you
Am                  F
   Did you hear the music
      G                Am
   A serenade from the stars

Am                 F   
   Wake up, wake up
        G           Am  
   Wake up and look around you
Am               F
   We're lost in space
           G           Am
   And the time is our own

[Chorus]
G        Dm        Am        Am
   Whoaaaaaaaaaaaaahoooooooooooooooo
G        Dm        Am        F        G        Am
   Whoaaaaaaaaaaaaahooooooooooooooooooooooooooo~ooooooo
Am       F         G         Am
Ahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh


[Verse 2]
Am                  F
   Did you feel the wind
         G        Am
   As it blew all around you
Am                  F
   Did you feel the love
        G          Am
   That was in the air

Am                 F
   Wake up, wake up
        G           Am
   Wake up and look around you
Am               F
   We're lost in space
           G           Am
   And the time is our own

[Chorus]
G        Dm        Am        Am
   Whoaaaaaaaaaaaaahoooooooooooooooo
G        Dm        Am        F        G        Am
   Whoaaaaaaaaaaaaahooooooooooooooooooooooooooo~ooooooo
Am       F         G         Am
Ahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh

[Verse 3]
Am               F
   The sun comes up
          G          Am   
   And it shines all around you
Am                F
   You're lost in space
           G             Am
   And the earth is your home


[Ending Chorus]
G         Dm         Am         Am
   Whoaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa