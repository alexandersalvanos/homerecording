Drivers Seat

Bm A 
Bm A G

Bm      A  G Bm                      A        G 
Doing alright, a little drivin' on a Saturday night
Bm            A G Bm               A    G
And come what may, gonna dance the day away

Bm A 
Bm A G

Bm        A  G  Bm                          A          G
Jenny was sweet,  she always smiles for the people she meets
Bm             A   G  Bm                      A          G
On trouble and strife, she had another way of looking at life.

Bm A 
Bm A G

Bm      A  G Bm                                     A      G
News is blue (the news is blue), has its own way to get to you
Bm         A  B Bm                                A         G
What can I do? (what can I do) When I remember my time with you

Bm A 
Bm A G

Bm           A  G Bm                  A            G
Pick up your feet, got to move to the trick of the beat
Bm      A  G Bm                           A        G
It�s no lead, just take your place in the driver's seat

Bm       A    G Bm             A     G
Driver's seat, oohoo, driver's seat, yeah
Bm       A    G Bm             A     G
Driver's seat, oohoo, driver's seat, yeah

Bm      A  G Bm                      A        G
Doing alright, a little drivin' on a Saturday night
Bm            A G Bm               A    G
And come what may, gonna dance the day away

Bm       A    G Bm             A     G
Driver's seat, oohoo, driver's seat, yeah
Bm       A    G Bm             A     G
Driver's seat, oohoo, driver's seat, yeah

Bm        A  G Bm            A     G
Jenny was sweet, there is no lead, yeah
Bm           A G Bm              A   G
Pick up your feet, pick up, pick up, yeah
Bm           A  G Bm               A    G
Pick up your feet, gonna dance the day away

Bm       A    G Bm             A     G
Driver's seat, oohoo, driver's seat, yeah
Bm       A    G Bm             A     G
Driver's seat, oohoo, driver's seat, yeah 2x

Bm A 
Bm A G